<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CCThis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccthis:crud {plural} {singular}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Can\'t CRUD This!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $replacements = [];
        $replacements['plural'] = strtolower($this->argument('plural'));
        $replacements['singular'] = strtolower($this->argument('singular'));
        $replacements['Plural'] = ucwords($replacements['plural']);
        $replacements['Singular'] = ucwords($replacements['singular']);


        // Create the view directory.
        // Jesus PHPStorm you crazy!
        $concurrentDirectory = base_path() . '/resources/views/' . $replacements['plural'];
        if ( ! is_dir($concurrentDirectory) && ! mkdir($concurrentDirectory, 0755, true) && ! is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        // Create the Services directory.
        // Jesus PHPStorm you crazy!
        $concurrentDirectory = base_path() . '/app/Services';
        if ( ! is_dir($concurrentDirectory) && ! mkdir($concurrentDirectory, 0755, true) && ! is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        // Create the Requests directory.
        // Jesus PHPStorm you crazy!
        $concurrentDirectory = base_path() . '/app/Http/Requests';
        if ( ! is_dir($concurrentDirectory) && ! mkdir($concurrentDirectory, 0755, true) && ! is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        // Create the test/Services directory.
        // Jesus PHPStorm you crazy!
        $concurrentDirectory = base_path() . '/tests/Services';
        if ( ! is_dir($concurrentDirectory) && ! mkdir($concurrentDirectory, 0755, true) && ! is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        $stubs = $this->getStubs();

//        $c = collect($stubs);
//        dd($c->pluck('path'));


        foreach ($stubs as $key => $stub) {
            foreach ($replacements as $search => $replace) {
                $stub['path'] = str_replace('%'.$search.'%', $replace, $stub['path']);
                $stub['content'] = str_replace('%'.$search.'%', $replace, $stub['content']);
            }
            file_put_contents(base_path() . '/' . $stub['path'], $stub['content']);
        }

        $this->newLine();
        $this->info("CRUD files have been created, now just a little sugar!");
        $this->newLine();
        $this->info('Add this to your routes:');
        $this->newLine();
        $this->line("     Route::resource('".$replacements['plural']."', ".$replacements['Plural']."Controller::class);");
        $this->newLine();
        $this->info('Add this to your navigation in layout.app:');
        $this->newLine();
        $this->line('     <li><a class="nav-link" href="{{ route(\''.$replacements['plural'].'.index\') }}">'.$replacements['Plural'].'</a></li>');
        $this->newLine();
        $this->info('Add this to your config/app.php \'providers\' => [');
        $this->newLine();
        $this->line('     App\Providers\\'.$replacements['Singular'].'ServiceProvider::class,');
        $this->newLine();
        $this->info('Add this to your config/app.php \'aliases\' => [');
        $this->newLine();
        $this->line('     \''.$replacements['Singular'].'Service\' => App\Services\\'.$replacements['Singular'].'Service::class,');


        return Command::SUCCESS;
    }


    private function getStubs(): array
    {
        $stubs = [];



        // CONTROLLER STUB
        $stubs['controller'] = [];
        $stubs['controller']['path'] = 'app/Http/Controllers/%Plural%Controller.php';

$content = <<<'EOD'
<?php

namespace App\Http\Controllers;

use App\Models\%Singular%;
use App\Services\%Singular%Service;
use App\Http\Requests\Store%Singular%Request;
use App\Http\Requests\Update%Singular%Request;
use App\Http\Requests\Destroy%Singular%Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class %Plural%Controller extends Controller
{
    protected %Singular%Service $%singular%Service;

    public function __construct(%Singular%Service $%singular%Service)
    {
        $this->%singular%Service = $%singular%Service;
    }

    public function index(Request $request)
    {
        $pp = 20;
        $%plural% = $this->%singular%Service->search($request->all())->paginate($pp);

        return view('%plural%.index', [
            '%plural%' => $%plural%
        ]);
    }

    public function create()
    {
        return view('%plural%.create');
    }

    public function store(Store%Singular%Request $request): RedirectResponse
    {
        $created = %Singular%::create($request->validated()); // All fields need to come from the request filtered.

        return redirect()->route('%plural%.index')
                         ->with('success', '%Singular% "' . $created->name . '" created successfully.');
    }

    public function show(%Singular% $%singular%)
    {
        return view('%plural%.show', [
            '%singular%' => $%singular%
        ]);
    }

    public function edit(%Singular% $%singular%)
    {
        return view('%plural%.edit', [
            '%singular%' => $%singular%
        ]);
    }

    public function update(Update%Singular%Request $request, %Singular% $%singular%): RedirectResponse
    {
        $%singular%->update($request->validated()); // All fields need to come from the request filtered.

        return redirect()->route('%plural%.index')
                         ->with('success', '%Singular% "' . $%singular%->name . '" updated successfully');
    }

    public function destroy(Destroy%Singular%Request $request, %Singular% $%singular%): RedirectResponse
    {
        $name = $%singular%->name;

        $%singular%->delete();

        return redirect()->route('%plural%.index')
                         ->with('success', '%Singular% "' . $name . '" deleted successfully');
    }
}

EOD;
        $stubs['controller']['content'] = $content;



        // MODEL STUB
        $stubs['model'] = [];
        $stubs['model']['path'] = 'app/Models/%Singular%.php';

$content = <<<'EOD'
<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $name
 * @method static create(array $validated)
 */
class %Singular% extends Model
{
    use HasFactory;
    use Sluggable;

    protected $guarded = [];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true
            ]
        ];
    }
}

EOD;
        $stubs['model']['content'] = $content;



        // Service STUB
        $stubs['service'] = [];
        $stubs['service']['path'] = 'app/Services/%Singular%Service.php';

$content = <<<'EOD'
<?php

namespace App\Services;

use App\Models\%Singular%;
use Illuminate\Database\Eloquent\Builder;

class %Singular%Service
{
    public function search(array $params): Builder
    {
        $%plural% = %Singular%::query()->latest();

        if ($params['search'] ?? '') {
            $%plural%->where('name', 'like', '%' . $params['search'] . '%');
        }

        return $%plural%;
    }
}

EOD;
        $stubs['service']['content'] = $content;



        // Service STUB
        $stubs['serviceprovider'] = [];
        $stubs['serviceprovider']['path'] = 'app/Providers/%Singular%ServiceProvider.php';

$content = <<<'EOD'
<?php

namespace App\Providers;

use App\Services\%Singular%Service;
use Illuminate\Support\ServiceProvider;

class %Singular%ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(%Singular%Service::class, function ($app) {
            return new %Singular%Service();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
EOD;
        $stubs['serviceprovider']['content'] = $content;



        // Factory STUB
        $stubs['factory'] = [];
        $stubs['factory']['path'] = 'database/factories/%Singular%Factory.php';

$content = <<<'EOD'
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class %Singular%Factory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
        ];
    }
}

EOD;
        $stubs['factory']['content'] = $content;



        // Seeder STUB
        $stubs['seeder'] = [];
        $stubs['seeder']['path'] = 'database/seeders/%Singular%Seeder.php';

$content = <<<'EOD'
<?php

namespace Database\Seeders;

use App\Models\%Singular%;
use Illuminate\Database\Seeder;

class %Singular%Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        %Singular%::factory()
            ->count(20)
            ->create();
    }
}


EOD;
        $stubs['seeder']['content'] = $content;



        // INDEX BLADE STUB
        $stubs['index'] = [];
        $stubs['index']['path'] = 'resources/views/%plural%/index.blade.php';

$content = <<<'EOD'
@extends('layouts.app')

@section('content')

    @include('shared.head', ['plural' => '%plural%', 'singular' => '%singular%'])

    @include('shared.alerts')

    <div class="row">
        <div class="col-8">&nbsp;</div>
        <form class="col-4">
            <div class="form-floating mb-3">
                <input class="form-control" type="search" id="searchInput" name="search" value="{{request('search')}}"/>
                <label for="searchInput">Search</label>
            </div>
        </form>
    </div>

    <div class="row">
      <div class="col-12">
        <table class="table table-striped">
          <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Slug</th>

                <th class="text-end">Action</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($%plural% as $%singular%)
                <tr>
                    <td>{{ $%singular%->id }}</td>
                    <td>{{ $%singular%->name }}</td>
                    <td>{{ $%singular%->slug }}</td>

                    <td class="text-end">
                        @include('shared.actions', ['plural' => '%plural%', 'model' => $%singular%])
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

    <div class="row">
        <div class="col-12">
            {!! $%plural%->links() !!}
        </div>
    </div>


@endsection

EOD;
        $stubs['index']['content'] = $content;



        // CREATE BLADE STUB
        $stubs['create'] = [];
        $stubs['create']['path'] = 'resources/views/%plural%/create.blade.php';

        $content = <<<'EOD'
@extends('layouts.app')

@section('content')

    @include('shared.head', ['plural' => '%plural%', 'singular' => '%singular%'])
    @include('shared.alerts')

    <div class="row">
        <div class="col-12">
            <form class="needs-validation" action="{{ route('%plural%.store') }}" method="POST" novalidate>
                @csrf
                @include('shared.forms.input', ['name' => 'name', 'label' => 'Name'])
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
@endsection

EOD;
        $stubs['create']['content'] = $content;



        // EDIT BLADE STUB
        $stubs['edit'] = [];
        $stubs['edit']['path'] = 'resources/views/%plural%/edit.blade.php';

$content = <<<'EOD'
@extends('layouts.app')

@section('content')

    @include('shared.head', ['plural' => '%plural%', 'singular' => '%singular%'])
    @include('shared.alerts')

    <div class="row">
        <div class="col-12">
            <form class="needs-validation" action="{{ route('%plural%.update', $%singular%->id) }}" method="POST" novalidate>
                @csrf
                @method('PUT')
                @include('shared.forms.input', ['name' => 'name', 'label' => 'Name', 'value' => $%singular%->name])
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection

EOD;
        $stubs['edit']['content'] = $content;



        // SHOW STUB
        $stubs['show'] = [];
        $stubs['show']['path'] = 'resources/views/%plural%/show.blade.php';

$content = <<<'EOD'
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <h2>{{ $%singular%->name }}</h2>

            <p>
            {{ $%singular%->slug }}
            </p>
        </div>
    </div>
@endsection

EOD;
        $stubs['show']['content'] = $content;



        // Store Request STUB
        $stubs['storerequest'] = [];
        $stubs['storerequest']['path'] = 'app/Http/Requests/Store%Singular%Request.php';

$content = <<<'EOD'
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store%Singular%Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:%plural%|max:255',
        ];
    }
}

EOD;
        $stubs['storerequest']['content'] = $content;



        // Update Request STUB
        $stubs['updaterequest'] = [];
        $stubs['updaterequest']['path'] = 'app/Http/Requests/Update%Singular%Request.php';

        $content = <<<'EOD'
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Update%Singular%Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:posts,name,'.$this->%singular%->id.'|max:255',
        ];
    }
}

EOD;
        $stubs['updaterequest']['content'] = $content;



        // Store Request STUB
        $stubs['destroyrequest'] = [];
        $stubs['destroyrequest']['path'] = 'app/Http/Requests/Destroy%Singular%Request.php';

$content = <<<'EOD'
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Destroy%Singular%Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}

EOD;
        $stubs['destroyrequest']['content'] = $content;



        // MIGRATION STUB
        $stubs['migration'] = [];
        try {
            $stubs['migration']['path'] = 'database/migrations/' . date('Y_m_d') . '_' . random_int(100000, 999999) . '_create_%plural%_table.php';
        } catch (\Exception $e) {
            $stubs['migration']['path'] = 'database/migrations/' . date('Y_m_d') . '_000000_create_%plural%_table.php';
        }

$content = <<<'EOD'
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create%Plural%Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('%plural%', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('slug')->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('%plural%');
    }
}

EOD;
        $stubs['migration']['content'] = $content;


        // Test STUB
        $stubs['test'] = [];
        $stubs['test']['path'] = 'tests/Services/%Singular%ServiceTest.php';

$content = <<<'EOD'
<?php

namespace Tests\Services;

use App\Models\%Singular%;
use App\Services\%Singular%Service;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class %Singular%ServiceTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;
    use WithFaker;

    protected %Singular%Service $%singular%Service;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->%singular%Service = app(%Singular%Service::class);
        parent::__construct($name, $data, $dataName);
    }


    /** @test */
    public function it_searches(): void
    {
        %Singular%::create([
            'name' => 'test'
        ]);

        $result = $this->%singular%Service->search(['search' => 'test']);
        self::assertEquals(1, $result->count());

        $result = $this->%singular%Service->search(['search' => 'micky']);
        self::assertEquals(0, $result->count());
    }
}

EOD;
        $stubs['test']['content'] = $content;

        // RETURN ALL STUBS
        return $stubs;
    }
}
