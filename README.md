# CCThis

Can't CRUD This!

## What Does this Do?

It creates stubs and starter files for doing quick crud in a Laravel project. Most importantly it sets up your laravel to have an admin area with as little to no modifications to the stock Laravel - Out of the Box (ootb) scaffolding.

## Screenshot

![Screenshot Baby!](screenshot.png)

## 55 Seconds!

https://gitlab.com/ccpub/ccthis/-/blob/main/demo.mp4

## How? TLDR;

If you have a basic Laravel from scratch already setup.

1. Download the artisan command ```CCThis.php``` to your ```app/Console/Commands/CCThis.php```
2. Then run ```php artisan ccthis:crud widgets widget```


## NEVER RUN THIS TWICE!

__This is a one way ticket__

It will overwrite your existing files! Looking at your Mathias, looking at you...

## The Results

- app/Http/Controllers/%Plural%Controller.php
- app/Models/%Singular%.php
- app/Services/%Singular%Service.php
- app/Providers/%Singular%ServiceProvider.php
- tests/Services/%Singular%Service.php
- database/factories/%Singular%Factory.php
- database/seeders/%Singular%Seeder.php
- resources/views/%plural%/index.blade.php
- resources/views/%plural%/create.blade.php
- resources/views/%plural%/edit.blade.php
- resources/views/%plural%/show.blade.php
- app/Http/Requests/Store%Singular%Request.php
- app/Http/Requests/Update%Singular%Request.php
- app/Http/Requests/Delete%Singular%Request.php
- database/migrations/2021_11_30_649351_create_%plural%_table.php


## How? Laravel From Scratch

The bulk of this howto comes from this post here: https://www.kreaweb.be/laravel-8-bootstrap-5/. 

Essentially what we are going to do is make a fresh laravel, get it to Bootstrap 5, create a ```/admin/*``` area, and start making CRUD sets.

### 13 Steps (5 minutes):

1. Start a new laravel project: ```laravel new project```

2. Bring in Laravel UI: ```composer require laravel/ui```

3. Setup the project for Bootstrap 5 and Auth Sccaffolding: ```php artisan ui bootstrap --auth```

4. Bring in more Bootstrap 5: ```npm install bootstrap@latest bootstrap-icons @popperjs/core```

5. Compile it: ```npm run build```

6. Create a DB, Configure your .env

7. Bring in the all goodness of Bootstrap in the ```app.scss```:

-- Place this at the end --
```scss
// Bootstrap
@import 'bootstrap/scss/bootstrap';
@import 'bootstrap/scss/functions';
@import 'bootstrap/scss/variables';
@import "bootstrap/scss/utilities";
@import 'bootstrap-icons/font/bootstrap-icons.css'; // WE LOVE ICONS!
```
Compile it again: ```npm run build```

8. Download and unpack the views this zip: https://gitlab.com/ccpub/ccthis/-/raw/main/shared.zip

These are shared blade templates that will make life easier. They will unzip to a directory "shared"

9. Change the ```RouteServiceProvider.php``` constant variable line 20: From ```public const HOME = '/home';``` to ```public const HOME = '/admin/home';```

10. Change the ```web.php``` routes file and create a shceme such as:

```php
Route::prefix('admin')->group(function () { // create and /admin grouping
    Auth::routes();
    
    Route::middleware('auth')->group(function () { // all routes in here need authentication
        Route::get('/home', [HomeController::class, 'index'])->name('home');

        // Routes to the CRUDS!
        //Route::resource('posts', PostsController::class);
        //Route::resource('widgets', WidgetsController::class);
    });
});
```

11. Let models be sluggable: ```composer require cviebrock/eloquent-sluggable```

12. Add a "container" class to the main in app.blade.php

![Main Container!](main-container.png)

13. Finalize ```php artisan migrate```

14. Goto https://project.test/admin/register, create an account that you will use for "admin-ing".

*GET PAID*

### The Result

You now have working Laravel with an "/admin" area, an admin user, and basic bootstrap / blade setup. 

You can now do ```php artisan ccthis:crud posts post``` or ```php artisan ccthis:crud dogs dog```

This will generate a controller, views, model, migration, stubs.

You then add to the ```web.php``` routes file a route to the controller as resource:

```Route::resource('posts', PostsController::class);``` prefereably in the "auth" middleware from before.

To Link to these routes add a link in the ```layouts/app.blade.php``` file.

About line 37:
```html
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                        <li><a class="nav-link" href="{{ route('posts.index') }}">Posts</a></li> <!-- here -->
                    </ul>
```

